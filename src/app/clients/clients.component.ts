import { Component, OnInit } from '@angular/core';
import { Client } from './client/client.model';

@Component({
  selector: 'mt-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  
  clients: Client[] = [
    {
      id:"flavio-carvalho",
      name: "Flávio Carvalho",
      sex: "Masculino",
      rating: 4.9,
      imagePath: "assets/img/clients/boy.png"
    },
    {
      id:"janaina-carvalho",
      name: "Janaína Carvalho",
      sex: "Feminino",
      rating: 5.0,
      imagePath: "assets/img/clients/girl.png"
    }
  ] 

  

  constructor() { }

  ngOnInit() {
  }

}
