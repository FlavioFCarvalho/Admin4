export interface Client{
    id: string
    name: string
    sex: string
    rating: number
    imagePath: string 
}