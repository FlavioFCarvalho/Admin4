import { Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ClientsComponent } from './clients/clients.component';
import { ProdutsComponent } from './produts/produts.component';

export const ROUTES: Routes = [
    {path: '', component: HomeComponent},
    {path: 'clients', component: ClientsComponent},
    {path: 'products', component: ProdutsComponent},
    {path: 'about', component: AboutComponent}

]