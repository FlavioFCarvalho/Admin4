export interface Product{
    id: string
    name: string
    description: string
    rating: number
    imagePath: string 
}