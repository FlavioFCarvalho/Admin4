import { Component, OnInit } from '@angular/core';
import { Product } from './product/product.model';

@Component({
  selector: 'mt-produts',
  templateUrl: './produts.component.html'
})
export class ProdutsComponent implements OnInit {

  products: Product[] =[
    {
      id:"hamburguer-fritas",
      name: "Hamburguer com Fritas",
      description: "Muito saboroso",
      rating: 4.2,
      imagePath: "assets/img/foods/burger.png"
    },
    {
      id:"sorvete_creme",
      name: "Sorvete de creme",
      description: "Delicia gelada",
      rating: 4.3,
      imagePath: "assets/img/foods/icestd.png"

    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
